<?php

namespace App\Http\Controllers;

use App\Models\rakyat;
use App\Models\kelahiran;
use App\Models\Kematian;
use App\Models\Struktur;
use App\Models\Perangkat;
use Illuminate\Http\Request;
use Illuminate\Http\Post;

class SimdesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = rakyat::latest();

        if (request('search')){
            $post->where('NIK','like','%'.request('search').'%');
        }

        $dtrakyat = rakyat::all();
        return view('Datarakyat_admin',compact('dtrakyat'));

    }
    public function test()
    {
        $post = Kelahiran::latest();

        if (request('search')){
            $post->where('no_id','like','%'.request('search').'%');
        }
        $dtrakyat = Kelahiran::all();
        return view('DKelahiran_admin',compact('dtrakyat'));
    }
    public function projeck()
    {
        $post = Kematian::latest();

        if (request('search')){
            $post->where('nama_lengkap','like','%'.request('search').'%');
        }
        $dtrakyat = Kematian::all();
        return view('DKematian_admin',compact('dtrakyat'));
    }
    public function struktur()
    {
        $dtrakyat = Struktur::all();
        return view('HlStruktur',compact('dtrakyat'));
    }
    public function tambah()
    {
        $dtrakyat = Struktur::all();
        return view('Struktur',compact('dtrakyat'));
    }
    public function perangkat()
    {
        $dtrakyat = Perangkat::all();
        return view('Hlperangkat',compact('dtrakyat'));
    }
    public function nambah()
    {
        $dtrakyat = Perangkat::all();
        return view('perangkat',compact('dtrakyat'));
    }
    // public function dataryt()
    // {
    //     return view('Datarakyat_admin');
    // }
    public function kelahiran()
    {
        return view('DtKelahiran');
    }
    public function kematian()
    {
        return view('DtKematian');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Data');
    }
    public function buat()
    {
        return view('DKelahiran');
    }
    public function make()
    {
        return view('DKematian');
    }
    public function struktur1()
    {
        return view('Struktur');
    }
    public function perangkat1()
    {
        return view('Perangkat');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    public function store(Request $request)
    {
        rakyat::create([
            'NIK'=> $request->no,
            'nama_lengkap'=> $request->nama,
            'jenis_kelamin'=> $request->jenis,
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,
            'alamat'=> $request->alamat,
            'agama'=> $request->agama,
            'pendidikan'=> $request->pendidikan,
            'pekerjaan'=> $request->pekerjaan,
        ]);
        return redirect('admin');
    }
    public function panggil(Request $request)
    {
        kelahiran::create([
            'no_id'=> $request->no,
            'nama_lengkap'=> $request->nama,           
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,
            'jenis_kelamin'=> $request->jenis,           
            'nama_ayah'=> $request->ayah,
            'nama_ibu'=> $request->ibu,
        ]);
        return redirect('saya');
    }
    public function call(Request $request)
    {
        kematian::create([            
            'nama_lengkap'=> $request->nama,           
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,
            'jenis_kelamin'=> $request->jenis,           
            'alamat'=> $request->alamat,
            'riwayat_kematian'=> $request->mati,
            'tanggal_wafat'=> $request->wafat,
        ]);
        return redirect('anda');
    }
    public function simpan_struktur(Request $request)
    {
        Struktur::create([            
            'no'=> $request->no,           
            'nama_lengkap'=> $request->nama,
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,           
            'jenis_kelamin'=> $request->jenis,
            'alamat'=> $request->alamat,
            'umur'=> $request->umur,
            'jabatan'=> $request->jabatan,
        ]);
        return redirect('struktur');
    }
    public function simpan_perangkat(Request $request)
    {
        Perangkat::create([            
            'no'=> $request->no,           
            'nama_lengkap'=> $request->nama,
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,           
            'jenis_kelamin'=> $request->jenis,
            'alamat'=> $request->alamat,
            'jabatan'=> $request->jabatan,
        ]);
        return redirect('perangkat');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = rakyat::find($id);
        return view('EditDtrakyat' , compact('data'));
    }
    public function ubah($id)
    {
        $data = Kelahiran::find($id);
        return view('EditDkelahiran' , compact('data'));
    }
    public function ganti($id)
    {
        $data = Kematian::find($id);
        return view('EditDkematian' , compact('data'));
    }
    public function obe($id)
    {
        $data = Struktur::find($id);
        return view('Editstruktur' , compact('data'));
    }
    public function aobe($id)
    {
        $data = Perangkat::find($id);
        return view('Editperangkat' , compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = rakyat::find($id);
        $save = $data->update([
            'NIK'=> $request->no,
            'nama_lengkap'=> $request->nama,
            'jenis_kelamin'=> $request->jenis,
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,
            'alamat'=> $request->alamat,
            'agama'=> $request->agama,
            'pendidikan'=> $request->pendidikan,
            'pekerjaan'=> $request->pekerjaan,
        ]);
        if($save){
            $dtsimdes = rakyat::all();
            return Redirect()->route('admin' , compact('dtsimdes'));
        }
    }
    public function rubah(Request $request, $id)
    {
        $data = Kelahiran::find($id);
        $save = $data->update([
            'no_id'=> $request->no,
            'nama_lengkap'=> $request->nama,
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,            
            'jenis_kelamin'=> $request->jenis,
            'nama_ayah'=> $request->ayah,
            'nama_ibu'=> $request->ibu,
        ]);
        if($save){
            $dtsimdes = Kelahiran::all();
            return Redirect()->route('saya' , compact('dtsimdes'));
        }
    }
    public function merubah(Request $request, $id)
    {
        $data = Kematian::find($id);
        $save = $data->update([
            'nama_lengkap'=> $request->nama,
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,            
            'jenis_kelamin'=> $request->jenis,
            'alamat'=> $request->alamat,
            'riwayat_kematian'=> $request->mati,
            'tanggal_wafat'=> $request->wafat,
        ]);
        if($save){
            $dtsimdes = Kematian::all();
            return Redirect()->route('anda' , compact('dtsimdes'));
        }
    }
    public function ngobe(Request $request, $id)
    {
        $data = Struktur::find($id);
        $save = $data->update([
            'no'=>$request->no,
            'nama_lengkap'=> $request->nama,
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,            
            'jenis_kelamin'=> $request->jenis,
            'alamat'=> $request->alamat,
            'umur'=> $request->umur,
            'jabatan'=> $request->jabatan,
        ]);
        if($save){
            $dtsimdes = Struktur::all();
            return Redirect()->route('struktur' , compact('dtsimdes'));
        }
    }
    public function ngubah(Request $request, $id)
    {
        $data = Perangkat::find($id);
        $save = $data->update([
            'no'=>$request->no,
            'nama_lengkap'=> $request->nama,
            'tempat_lahir'=> $request->tempat,
            'tanggal_lahir'=> $request->tanggal,            
            'jenis_kelamin'=> $request->jenis,
            'alamat'=> $request->alamat,
            'jabatan'=> $request->jabatan,
        ]);
        if($save){
            $dtsimdes = Perangkat::all();
            return Redirect()->route('perangkat' , compact('dtsimdes'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pes =rakyat::findorfail($id);
        $pes->delete();
        return back();
    }
    public function dest($id)
    {
        $pes =Kelahiran::findorfail($id);
        $pes->delete();
        return back();
    }
    public function destry($id)
    {
        $pes =Kematian::findorfail($id);
        $pes->delete();
        return back();
    }
    public function desty($id)
    {
        $pes =Struktur::findorfail($id);
        $pes->delete();
        return back();
    }
    public function destr($id)
    {
        $pes =Perangkat::findorfail($id);
        $pes->delete();
        return back();
    }
}