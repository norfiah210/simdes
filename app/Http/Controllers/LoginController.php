<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.index',[
            'title' => 'login'
        ]);
    }
    public function home_admin()
    {
        return view('HomeAdmin');
    }
    public function authenticate(Request $request)
    {
        $credentials=$request->validate([
            'email'=>'required|email:dns',
            'password'=>'required' 
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/home_admin');
        }
        return back()->with('loginError','Login filed!');
    }
    public function logout(Request $request)
    {
        Auth:: logout();

        $request -> session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}
