<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelahiran extends Model
{
    protected $table ="kelahirans";
    protected $primaryKey ="id";
    protected $fillable = [
                'id','no_id','nama_lengkap','tempat_lahir','tanggal_lahir','jenis_kelamin','nama_ayah','nama_ibu'
    ];
}
