<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kematian extends Model
{
    protected $table ="kematians";
    protected $primaryKey ="id";
    protected $fillable = [
                'id','nama_lengkap','tempat_lahir','tanggal_lahir','jenis_kelamin','alamat','riwayat_kematian','tanggal_wafat'
    ];
}
