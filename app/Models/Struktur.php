<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Struktur extends Model
{
    protected $table ="Strukturs";
    protected $primaryKey ="id";
    protected $fillable = [
    'id','no','nama_lengkap','tempat_lahir','tanggal_lahir','jenis_kelamin','alamat','umur','jabatan'
    ];
}
