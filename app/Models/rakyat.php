<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rakyat extends Model
{
    protected $table ="rakyats";
    protected $primaryKey ="id";
    protected $fillable = [
                'id','NIK','nama_lengkap','jenis_kelamin','tempat_lahir','tanggal_lahir','alamat','agama','pendidikan','pekerjaan'
    ];
}
