<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStruktursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strukturs', function (Blueprint $table) {
            $table->id();
            $table->string('no',50);
            $table->string('nama_lengkap',100);
            $table->string('tempat_lahir',50);
            $table->date('tanggal_lahir',50);
            $table->string('jenis_kelamin',50);
            $table->string('alamat',100);
            $table->string('umur',100);
            $table->string('jabatan',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strukturs');
    }
}
