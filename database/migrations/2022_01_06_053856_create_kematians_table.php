<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKematiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kematians', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lengkap',100);
            $table->string('tempat_lahir',100);
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin',100);
            $table->string('alamat',100);
            $table->string('riwayat_kematian',100);
            $table->date('tanggal_wafat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kematians');
    }
}
