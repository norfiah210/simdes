<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRakyatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rakyats', function (Blueprint $table) {
            $table->id();
            $table->string('NIK',50);
            $table->string('nama_lengkap',100);
            $table->string('jenis_kelamin',50); 
            $table->string('tempat_lahir',100);
            $table->date('tanggal_lahir');
            $table->string('alamat',100);
            $table->string('agama',100);
            $table->string('pendidikan',100);
            $table->string('pekerjaan',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rakyats');
    }
}
