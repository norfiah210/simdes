<?php

use App\Http\Controllers\SimdesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::get('home', function () {
    return view('Home');
});

Route::get('/login', [LoginController:: class, 'index']);
// Route::get('/home', [LoginController:: class, 'home']);
Route::get('/home_admin', [LoginController:: class, 'home_admin']);
Route::post('/login', [LoginController:: class, 'authenticate']);

Route::get('/register', [RegisterController:: class, 'index']);

Route::post('/register', [RegisterController:: class, 'store']);



Route::get('rakyat', function () {
    return view('Data');
});

Route::get('admin',[SimdesController::class,"index"])->name('admin');
Route::get('saya',[SimdesController::class,"test"])->name('saya');
Route::get('anda',[SimdesController::class,"projeck"])->name('anda');

Route::get('lahir',[SimdesController::class,"kelahiran"])->name('Dlahir');
Route::get('mati',[SimdesController::class,"kematian"])->name('Dkematian');

Route::get('/Data',[SimdesController::class, "create"])->name ('Data') ;
Route::get('/simpan',[SimdesController::class,"store"])->name('simpan');
Route::get('/hapus/{id}',[SimdesController::class,"destroy"])->name('hapus');
Route::get('/EditDtrakyat/{id}',[SimdesController::class,"edit"])->name('EditDtrakyat');
Route::put('/edit/{id}',[SimdesController::class,"update"])->name('edit');
Route::get('/cari',[SimdesController::class,"search"])->name('cari');

Route::get('/DKelahiran',[SimdesController::class, 'buat'])->name ('DKelahiran') ;
Route::get('/save',[SimdesController::class,"panggil"])->name('save');
Route::get('/delete/{id}',[SimdesController::class,"dest"])->name('delete');
Route::get('/EditDkelahiran/{id}',[SimdesController::class,"ubah"])->name('EditDkelahiran');
Route::put('/ubah/{id}',[SimdesController::class,"rubah"])->name('ubah');

Route::get('/DKematian',[SimdesController::class, 'make'])->name ('DKematian') ;
Route::get('/saved',[SimdesController::class,"call"])->name('saved');
Route::get('/cut/{id}',[SimdesController::class,"destry"])->name('cut')->middleware('admin');
Route::get('/EditDkematian/{id}',[SimdesController::class,"ganti"])->name('EditDkematian');
Route::put('/ganti/{id}',[SimdesController::class,"merubah"])->name('ganti');

Route::get('/struktur',[SimdesController::class,"struktur"])->name('struktur');
Route::get('/struktur1',[SimdesController::class,"struktur1"])->name('struktur1');
Route::get('/simpan_struktur',[SimdesController::class,"simpan_struktur"])->name('simpan_struktur');
Route::get('/struktur2',[SimdesController::class,"tambah"])->name('struktur2');
Route::get('/Editstruktur/{id}',[SimdesController::class,"obe"])->name('Editstruktur');
Route::put('/obe/{id}',[SimdesController::class,"ngobe"])->name('obe');
Route::get('/buang/{id}',[SimdesController::class,"desty"])->name('buang');

Route::get('/perangkat',[SimdesController::class,"perangkat"])->name('perangkat');
Route::get('/perangkat1',[SimdesController::class,"perangkat1"])->name('perangkat1');
Route::get('/simpan_perangkat',[SimdesController::class,"simpan_perangkat"])->name('simpan_perangkat');
Route::get('/Editperangkat/{id}',[SimdesController::class,"aobe"])->name('Editperangkat');
Route::put('/aobe/{id}',[SimdesController::class,"ngubah"])->name('aobe');
Route::get('/throw/{id}',[SimdesController::class,"destr"])->name('throw');
Route::get('/perangkat2',[SimdesController::class,"nambah"])->name('perangkat2');

// Route::get('dua',[SimdesController::class,"dataryt"])->name('ryt');
// Route::get('/simpan',[SimdesController::class,"store"])->name('simpan');


Route::get('/Profile_admin', function () {
    return view('ProfileAdmin');
});
Route::get('/Profile', function () {
    return view('profile');
});
Route::post('logout',[LoginController::class,"logout"])->name('logout');
// Route::get('/satu', function () {
//     return view('Data');
// });

// Route::get('dua',[SimdesController::class,"dataryt"])->name('ryt');
// Route::get('/simpan',[SimdesController::class,"store"])->name('simpan');

