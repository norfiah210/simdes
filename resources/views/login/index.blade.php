@extends('layouts.main1')

@section('container')

<h1></h1>

<main class="form-signin">
    <form action="/login" method="POST">
      @csrf
      <center>
      <div>
      <h1 class="h3 mb-3 fw-normal">Please Login</h1>
    </div>
  </center>
      <div class="form-floating">
        <input type="email" class="form-control" name="email" id="email" placeholder="name@example.com" required>
        <label for="email">Email address</label>
      </div>

      <div class="form-floating">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
        <label for="password">Password</label>
      </div>
  
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
    </form>
    <small>Not Register ? <a href="register">Register Now!</a> </small>
  </main>
  
@endsection