


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/styles.css">
     <link rel="stylesheet" href="CSS/ionicons.min.css">
    <title>SIMDES</title>
  </head>
  <body>

   <div>
       
   </div>

   <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
   <h1 style="color: white; margin-left:250px; font-style:italic ">SISTEM DESA</h1>
        <ul class="navbar-nav ">
            <li class="nav-item">
                <a href="/login" class="nav-linnk" style="margin-left: 550px"><button type="button" class="btn btn-dark">LOGIN</button></a>
            </li>
            <li class="nav-item">
                <a href="#features" class="nav-linnk" style="margin-left: 30px"><button type="button" class="btn btn-dark">ABOUT</button></a>
            </li>
        </ul>
      </div>
    </div>
  </nav>
  <center width="500px" heigh="500px">
<section style="background-color: gray;height:700px" >
            <div class="text py-5">
                <img src="img/logo1.jpeg" alt=logo desa width="500px" style="padding-top: 20px">
                 <h1 style="padding-top: 10px">Aplikasi</h1>
                <h2 style="font-style:italic">Input <span class="bold">Data</span> Keseluruhan <span class="bold">Rakyat</span>.</h2>
                <div class="cta">
                    <a href="home" class="btn btn-dark smooth-link">Get Started</a>
                </div>
                
            </div>
        </section>
</center>
<center>
        <section class="padding" id="features">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4 col-sm-12">
                        <div class="list-item">
                            <div class="icon">
                                <i class="ion-code"></i>
                            </div>
                            <div class="desc">
                                <h2>Deskripsi</h2>
                                <p>
                                    aplikasi ini berfungsi untuk menginputkan data Keseluruhan rakyat, data kelahiran dan data kematian rakyat di desa lesong daya sehingga dapat mempermudah operator desa 
                                    dan perangkat desa dalam merekap data rakyat.
                                </p>								
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-sm-12">
                        <div class="list-item no-spacing">
                            <div class="icon">
                                <i class="ion-social-github"></i>
                            </div>
                            <div class="desc">
                                <h2>Petunjuk Penggunaan</h2>
                                <p>
                                    jika anda user maka anda cukup mengklik tombol "Get Started", maka secara otomatis anda akan dilarikan ke halaman home dari user.
                                    jika anda Admin maka anda mengklik tombol login, maka secara otomatis akan di larikan ke halaman Login, disana anda harus register terlebih dahulu,
                                    setelah itu baru anda bisa login menggunakan akun yang anda daftarkan. setelah klik login secara otomatis anda akan masuk ke home admin.   
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-sm-12">
                        <div class="list-item no-spacing">
                            <div class="icon">
                                <i class="ion-paintbrush"></i>
                            </div>
                            <div class="desc">
                                <h2>Pengenalan Aplikasi</h2>
                                <p>
                                    Aplikasi ini digunakan oleh dua pengguna yaitu user dan admin, yang mana yang jadi user yaitu bagian kepala dusun sedangkan yang jadi admin yaitu operator desa. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </center>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <footer style="margin-top: 10px ;background-color: black" >
      <div class="container" style="height: 100px ;padding-top:30px"  >
        
        <small style="color: white; margin-left:200px"><img src="img/fb.png" width="30px" height="30px" style="border-radius: 50px;margin-right: 10px">@lesongdaya_hebat</small>
        <small style="color: white; margin-left:100px"><img src="img/instagram.jpg" width="30px" height="30px" style="border-radius: 50px;margin-right: 10px">@lesongdaya_hebat</small>
        <small style="color: white; margin-left:100px"><img src="img/wa.jpg" width="25px" height="25px" style="border-radius: 50px;margin-right: 10px">082335947226</small>
      </div>
      </footer>  
   
  </body>
</html>


