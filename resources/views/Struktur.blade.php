<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/style.css">
    
  </head>
  <body>

  
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img src="img/logo.jpg" alt="logo" width="100" height="100">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">SIMDES</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="home_admin">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Profile_admin">Profile</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="/struktur" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Struktur
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">                 
                  <li><a class="dropdown-item" href="/struktur">Anggota BPD</a></li>
                  <li><a class="dropdown-item" href="/perangkat">Perangkat Desa</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="rakyat" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Data
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li><a class="dropdown-item" href="admin">Data Rakyat</a></li>
                  <li><a class="dropdown-item" href="saya">Data Kelahiran</a></li>
                  <li><a class="dropdown-item" href="anda">Data Kematian</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <form action="/logout" method="POST">
                @csrf
                {{-- <a class="nav-link" href="#" style="margin-left: 650px">Logout</a> --}}
                <button type="submit" class="btn btn-dark" style="margin-left: 750px">Logout</button>
                </form>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <section>
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="alert alert-primary text-center mt-3">Inputkan Data Anggota BPD</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('simpan_struktur')}}" method="get">
                        {{ csrf_field()}}
                    <table style="margin-left: 100px">
                      <tr>
                        <td style="width: 150px">NO</td>
                        <td style="width: 10px">:</td>
                        <td><input type="text" id="no" name="no" class="form-control" placeholder="No"></td>
                      </tr>
                      <tr>
                        <td>Nama Lengkap</td>
                        <td>:</td>
                        <td><input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Lengkap"></td>                        
                      </tr>
                      <tr>
                        <td>Tempat Lahir</td>
                        <td>:</td>
                        <td> <input type="text" id="tempat" name="tempat" class="form-control" placeholder="Tempat Lahir"></td>
                        </tr>
                      <tr>
                        <td>Tanggal Lahir</td>
                        <td>:</td>
                        <td><input type="date" id="tanggal" name="tanggal" class="form-control" ></td>
                      </tr>                      
                      <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td> <input type="text" id="jenis" name="jenis" class="form-control" placeholder="Jenis Kelamin"></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td> <input type="text" id="alamat" name="alamat" class="form-control" placeholder="Alamat"></td>
                        </tr>
                        <tr>
                            <td>Umur</td>
                            <td>:</td>
                            <td> <input type="text" id="umur" name="umur" class="form-control" placeholder="Umur"></td>
                            </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td>:</td>
                            <td> <input type="text" id="jabatan" name="jabatan" class="form-control" placeholder="Jabatan"></td>
                        </tr>    
                     </table>    
                   
                        <div class="from-group mt-2">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </section>

    
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <footer style="margin-top: 10px">
        <div class="container" style="height: 50px" >
          
          <small style="color: white; margin-left:200px""><img src="img/fb.png" width="50px" height="50px">@lesongdaya_hebat</small>
          <small style="color: white; margin-left:100px"><img src="img/instagram.png" width="30px" height="30px" style="margin-right: 10px">@lesongdaya_hebat</small>
          <small style="color: white; margin-left:100px""><img src="img/wa.jpg" width="30px" height="30px" style="margin-right: 10px">082335947226</small>
        </div>
        </footer> 
   
  </body>
</html>