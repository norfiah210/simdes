@extends('layouts.main1')

@section('container')

<main class="form-registrasion">
  <h1 class="h3 mb-3 fw-normal">Registraion Form </h1>
    <form action="/register"  method="post">
      @csrf
      <div class="form-floating">
        <input type="text" name="name" class="form-control @error('name') is-invalide @enderror " id="name" placeholder="name" required value="{{ old('name') }}">
        <label for="name">name</label>
        @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-floating">
        <input type="text" name="username" class="form-control @error('username') is-invalid @enderror " id="username" placeholder="username" required value="{{ old('username') }}">
        <label for="username">username</label>
        @error('username')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-floating">
        <input type="email" name="email" class="form-control @error('email') is-invalide @enderror" id="email" placeholder="name@example.com" required required value="{{ old('email') }}">
        <label for="email">Email address</label>
        @error('email')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-floating">
        <input type="password" name="password" class="form-control @error('password') is-invalide @enderror" id="password" placeholder="Password" required required value="{{ old('password') }}">
        <label for="password">Password</label>
        @error('password')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="w-100 btn btn-lg btn-primary" type="submit">Register</button>
    </form>
    <small>Already Register ? <a href="/">login</a> </small>
  </main>
  
@endsection