<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/style.css">
   <style>
     .scroll{
       height: 400px;
       overflow: scroll;
     }
   </style>
    <title>SIMDES</title>
  </head>
  <body>

   <div>
       
   </div>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img src="img/logo.jpg" alt="logo" width="100" height="100">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">SIMDES</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="home_admin">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Profile_admin">Profile</a>
              </li>              
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="data" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Struktur
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">                 
                  <li><a class="dropdown-item" href="/struktur">Anggota BPD</a></li>
                  <li><a class="dropdown-item" href="/perangkat">Perangkat Desa</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="data" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Data
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li><a class="dropdown-item" href="admin">Data Rakyat</a></li>
                  <li><a class="dropdown-item" href="saya">Data Kelahiran</a></li>
                  <li><a class="dropdown-item" href="anda">Data Kematian</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <form action="/logout" method="POST">
                @csrf
                {{-- <a class="nav-link" href="#" style="margin-left: 650px">Logout</a> --}}
                <button type="submit" class="btn btn-dark" style="margin-left: 750px">Logout</button>
                </form>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <section>
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="alert alert-primary text-center mt-3">Data Anggota BPD</h3>
                    <a href="{{ url('/struktur2')}}" class="btn btn-success ">TAMBAH<i class="fas fa-plus-square"></i></a>
                </div>
                
            </div>
        </div>
    </section>
    
    <div class="scroll">
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>NO</th>
                <th>Nama Lengkap</th>               
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Jenis Kelamin</th>                
                <th>Alamat</th>
                <th>Umur</th>
                <th>Jabatan</th>
                <th>Aksi</th>
            </tr>
            @foreach ($dtrakyat as $item)
            <tr>
                <!-- <td>{{$item->id}}</td> -->
                <td>{{$item->no}}</td>
                <td>{{$item->nama_lengkap}}</td>                
                <td>{{$item->tempat_lahir}}</td>
                <td>{{ $item->tanggal_lahir }}</td>
                <td>{{$item->jenis_kelamin}}</td>                
                <td>{{ $item->alamat }}</td>
                <td>{{ $item->umur }}</td>
                <td>{{ $item->jabatan }}</td>
                <td>
                <a href="{{ url('Editstruktur',$item->id) }}"><button class="btn btn-info btn-sm" > EDIT </button></a>
                <a href="{{ url('buang',$item->id) }}"><button class="btn btn-danger btn-sm" > HAPUS </button></a>
                {{-- <form method="POST" action="#" id="hapus">
                  @csrf
                  @method('DELETE')
                  <div class="d-grid gap-2">
                  <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('yakin?')">Hapus</button>
                  </div>
              </form>  --}}
          
                </td>
            </tr>
            @endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </table>
      </div>
    </div>
     
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
    
    
    <footer style="margin-top: 10px">
        <div class="container" style="height: 50px" >
          
          <small style="color: white; margin-left:200px""><img src="img/fb.png" width="50px" height="50px">@lesongdaya_hebat</small>
          <small style="color: white; margin-left:100px"><img src="img/instagram.png" width="30px" height="30px" style="margin-right: 10px">@lesongdaya_hebat</small>
          <small style="color: white; margin-left:100px""><img src="img/wa.jpg" width="30px" height="30px" style="margin-right: 10px">082335947226</small>
        </div>
        </footer> 
   
  </body>
</html>