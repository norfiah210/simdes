<?php

use App\Http\Controllers\SimdesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/Profile_admin', function () {
    return view('ProfileAdmin');
});
Route::get('/tiga', function () {
    return view('profile');
});
Route::get('/satu', function () {
    return view('Data');
});

Route::get('dua',[SimdesController::class,"dataryt"])->name('ryt');
Route::get('/simpan',[SimdesController::class,"store"])->name('simpan');
