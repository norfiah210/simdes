<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/style.css">
    <title>Hello, world!</title>
  </head>
  <body>

   <div>

   </div>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img src="img/logo.jpg" alt="logo" width="100" height="100">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">SIMDES</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Prof">Profile</a>
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="satu" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Data
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li><a class="dropdown-item" href="satu">Data Rakyat</a></li>
                  <li><a class="dropdown-item" href="#">Data Kelahiran</a></li>
                  <li><a class="dropdown-item" href="#">Data Kematian</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <h5 style="margin-left:30px; padding-Top:5px;">A. Profil Desa Lesong Daya</h5>
      <p style="margin-left:55px;fontSize: 12px;">Desa lesong daya merupakan salah satu desa yang termasuk dalam organisasi kecamatan batumarmar
         yang terletak di pantai utara pulau Madura menurut letak geografisnya. Desa lesong daya tersebut terletak jauh dari hiruk-pikuk perkotaan namun tidak menjadikannya sebagai
         daerah yang tertinggal. Sebuah desa kecil yang termasuk kawasan kabupaten pamekasan. Dengan memiliki jumlah penduduk 3.823 orang dengan rincian 1.811 laki-laki dan 2.012
         perempuan, dengan memiliki luas 6,01 km2 , desa tersebut memiliki 5 dusun di antaranya: Dusun Sangoleng, Dusun Brumbung, Dusun Jurang Butoh, Dusun Tobelle dan Dusun Tenga.
        </p>
    <h5 style="margin-left:30px; padding-Top:5px;">B. Sejarah Pemerintahan Desa</h5>
        <p style="margin-left:55px;fontSize: 12px;">Sejarah pemerintahan desa lesong daya tidak terlepas dari sejarah masyarakat, konon menurut cerita
        para sesepuh desa pada zaman kerajaan yang ada di kampung ini yaitu ada kaitannya dengan kerajaan pamekasan, kerajaan sampang, kerajaan sumenep dan kerajaan bangkalan dalam menghadapi
        penjajah pada kala itu keempat kerajaan tersebut bergabung untuk mengadakan musyawarah untuk mengatur strategi peperangan, pertahanan dan perdagangan yang bertempat di desa lesong daya.
        Desa lesong daya merupakan desa yang di pimpin pertama kali oleh bapak Singo laut, Syafiuddin, Asma’uddin, Muhari, Matsudin dan Arief budiatno yang memimpin pemerintahan desa hingga saat ini.
        </p>
    <h5 style="margin-left:30px; padding-Top:5px;">C. Kondisi Geografis Desa</h5>
        <p style="margin-left:55px;fontSize: 12px;">Luas wilayah desa lesong daya adalah 6,01 km2, jarak dari pemerintahan desa ke pusat pemerintahan kecamatan 7 km,
        sedangkan jarak dari pusat desa ke ibu kota kabupaten 44 km. Geografis wilayah desa lesong daya meliputi perbatasan desa:
        </p>
        <p style="margin-left:60px;fontSize: 12px;">1. Sebelah utara berbatasan dengan laut jawa</p>
        <p style="margin-left:60px;fontSize: 12px;">2. Sebelah timur berbatasan dengan desa kapong dan desa ponjanan barat</p>
        <p style="margin-left:60px;fontSize: 12px;">3. Sebelah selatan berbatasan dengan desa lesong laok</p>
        <p style="margin-left:60px;fontSize: 12px;">4. Sebelah barat berbatasan dengan desa batubintang</p>
        <p style="margin-left:55px;fontSize: 12px;">Mengingat letak desa lesong daya dalam pengelolaan pembangunan merupakan pertemuan antara pelaksanaan pembangunan
        dari berbagai sector dengan pembangunan swadaya  masyarakat. Sementara itu desa lesong daya merupakan unit pemerintahan paling selatan yang berbatasan dengan desa lesong laok dalam menunjang
        pelaksanaan tugas penyelenggaraan pemerintahan daerah. Dengan mempertimbangkan posisi desa lesong daya yang demikian strategis, maka kapasitas managemen pembangunan di desa lesong daya merupakan
        factor yang sangat penting dalam menentukan arah kebijakan dan kesejahteraan masyarakat desa lesong daya.</p>
    <h5 style="margin-left:30px; padding-Top:5px;">D. Demografis/Kependudukan Desa</h5>
        <p style="margin-left:55px;fontSize: 12px;">Berdasarkan data administrasi pemerintahan desa, jumlah penduduk yang tercatat secara administrasi, jumlah total 3.823 jiwa
        dari 872 kepala keluarga. Dengan rincian penduduk berjenis kelamin laki-laki berjumlah 1.811 jiwa, sedangkan yang berjenis kelamin perempuan berjumlah 2.012 jiwa. Survey data sekunder dilakukan oleh
        kader pembangunan desa, di maksudkan sebagai data pembanding dari data yanf ada di pemerintahan desa.</p>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <footer  class="margin-top:10px">
        <div class="container" style="height:50px; padding-top:10px">

            <small style="color: white; margin-left:200px"> <img src="img/fb.jpg" width="30px" height="30px" style="margin_right: 10px;">@lesongdaya_hebat</small>
            <small style="color: white; margin-left:100px"> <img src="img/instagram.jpg" width="30px" height="30px" style="margin_right: 10px;">@lesongdaya_hebat</small>
            <small style="color: white; margin-left:100px"> <img src="img/wa.jpg" width="30px" height="30px" style="margin_right: 10px;">082335947226</small>
        </div>
    </footer>

  </body>
</html>
