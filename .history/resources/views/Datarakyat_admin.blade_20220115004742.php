<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/style.css">
    <link rel="stylesheet" href="{{ route("satu") }}">
    <title>Hello, world!</title>
  </head>
  <body>

   <div>

   </div>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img src="img/logo.jpg" alt="logo" width="100" height="100">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">SIMDES</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Profile">Profile</a>
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="data" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Data
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li><a class="dropdown-item" href="data">Data Rakyat</a></li>
                  <li><a class="dropdown-item" href="#">Data Kelahiran</a></li>
                  <li><a class="dropdown-item" href="#">Data Kematian</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <section>
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="alert alert-primary text-center mt-3">Data Keseluruhan Rakyat</h3>
                    <a href="{{ route('Data')}}" class="btn btn-success ">PESAN<i class="fas fa-plus-square"></i></a>
                </div>

            </div>
        </div>
    </section>


    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>NIK</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>
                <th>Agama</th>
                <th>Pendidikan</th>
                <th>Pekerjaan</th>
                <th>Aksi</th>
            </tr>
            @foreach ($dtsimdes as $item)
            <tr>
                <!-- <td>{{$item->id}}</td> -->
                <td>{{$item->NIK}}</td>
                <td>{{$item->nama_lengkap}}</td>
                <td>{{$item->jenis_kelamin}}</td>
                <td>{{$item->tempat_lahir}}</td>
                <td>{{ $item->tanggal_lahir }}</td>
                <td>{{ $item->alamat }}</td>
                <td>{{ $item->agama }}</td>
                <td>{{ $item->pendidikan }}</td>
                <td>{{ $item->pekerjaan }}</td>
                <td>
                <a href="{{ route('edit', $item->id) }}"><button class="btn btn-info btn-sm" > EDIT </button></a>
                <a href="tiga"><button class="btn btn-success btn-sm" > BAYAR </button></a>
                <form method="POST" action="{{ route('delete', $item->id) }}" id="hapus">
                  @csrf
                  @method('DELETE')
                  <div class="d-grid gap-2">
                  <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('yakin?')">Hapus</button>
                  </div>
              </form>

                </td>
            </tr>
            @endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </table>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


  </body>
</html>
